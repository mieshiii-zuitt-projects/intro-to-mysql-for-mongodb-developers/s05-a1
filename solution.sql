1: 
SELECT customerName FROM customers WHERE country = "Philippines";

2:
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

3:
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

4:
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

5:
SELECT customerName FROM customers WHERE state IS NULL;

6:
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

7:
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

8:
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

9:
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

10:
SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

11:
SELECT DISTINCT country FROM customers;

12:
SELECT DISTINCT status FROM orders;

13:
SELECT customerName, country FROM customers WHERE country = "USA" OR country = "FRANCE" OR country = "CANADA";

14:
SELECT firstName, lastName, city FROM employees INNER JOIN offices ON employees.officeCode = offices.officeCode WHERE employees.officeCode = 5;

15:
SELECT customerName FROM customers INNER JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

16:
SELECT productName, customerName FROM customers INNER JOIN orders ON customers.customerNumber = orders.customerNumber 
INNER JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber 
INNER JOIN products ON orderdetails.productCode = products.productCode WHERE customerName = "Baane Mini Imports";


17:
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees INNER JOIN offices ON employees.officeCode = offices.officeCode  
INNER JOIN customers ON offices.country = customers.country;

18:
SELECT employees.firstName, employees.lastName FROM employees WHERE reportsTo = (SELECT employeeNumber FROM employees WHERE firstName = "Anthony" AND lastName = "Bow");

19:
SELECT productName, MSRP FROM products WHERE MSRP = (SELECT MAX(MSRP) FROM products);

20:
SELECT COUNT(*) FROM customers WHERE country = "UK";

21:
SELECT COUNT(productLine) AS Number, productLine FROM products GROUP BY productLine;

22:
SELECT employees.firstName, employees.lastName, COUNT(customerNumber) AS "Number of Customers Served" FROM customers INNER JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber GROUP BY salesRepEmployeeNumber;

23:
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;
